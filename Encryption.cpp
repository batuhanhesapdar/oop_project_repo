/*!
 *  @file   Encryption.cpp
 *  @author Berkay �nk - 152120181110
 *  @date   23th January 2021
 *  @brief  Definition of operations Encryption.cpp
 *
 */
#include"Encryption.h"


  /*!
   * Encrypts access code when called
   *
   * @param x takes our access code to encrypt
   * @return returns x our integer that takes code
   */
int Encryption::encrypt(int x)
{

	int first;
	int second;
	int third;
	int fourth;
	int temp;

	temp = x;
	first = (temp / 1000) % 10;
	temp = temp % 1000;
	second = (temp / 100) % 10;
	temp = temp % 100;
	third = (temp / 10) % 10;
	temp = temp % 10;
	fourth = (temp) % 10;

	first = (first + 7) % 10;
	second = (second + 7) % 10;
	third = (third + 7) % 10;
	fourth = (fourth + 7) % 10;

	x = third * 1000 + fourth * 100 + first * 10 + second;
	cout << "Encrypted access code is: " << x << endl;
	return x;
}


 /*!
  * Decrypts access code when called
  *
  * @param y takes our access code to decrypt
  * @return returns y our integer that takes code
  */
int Encryption::decrypt(int y)
{
	int first;
	int second;
	int third;
	int fourth;
	int temp;

	temp = y;
	first = (temp / 1000) % 10;
	temp = temp % 1000;
	second = (temp / 100) % 10;
	temp = temp % 100;
	third = (temp / 10) % 10;
	temp = temp % 10;
	fourth = (temp) % 10;

	first = (first + 3) % 10;
	second = (second + 3) % 10;
	third = (third + 3) % 10;
	fourth = (fourth + 3) % 10;

	y = third * 1000 + fourth * 100 + first * 10 + second;
	cout << "Decrypted access code is: " << y << endl;
	return y;
}