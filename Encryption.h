/*!
 *  @file   Encryption.h
 *  @author Berkay �nk - 152120181110
 *  @date   23th January 2021
 *  @brief  Declaration of class Encryption
 *
 */
#pragma once
#include<iostream>

using namespace std;

class Encryption {
public:
	int encrypt(int);
	int decrypt(int);
};