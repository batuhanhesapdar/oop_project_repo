﻿/*!
 *  @file   LaserSensor.cpp
 *  @brief  Including Laser Sensor operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-01-20
 */
#include "LaserSensor.h"
	using namespace std;

/*!
 * Get distance information of sensor with index
 * @param index index of array
 * @return returns distance of sensor
 */
float LaserSensor::getRange(int index)
{
	robotAPI->getLaserRange(ranges);
	return ranges[index];
}
/*!
 * Uploads the robot's current sensor distance values to the ranges array.
 * @param hold[] the array of current sensor distance
 *
 */
void LaserSensor::updateSensor(float hold[])
{
	ranges = new float[541];
	hold = new float[541];
	robotAPI->getLaserRange(ranges);
	for (int i = 0; i < 541; i++) {
		hold[i] = ranges[i];
	}
}

/*!
 * Get maximum distance information of sensor with index
 * @param index index of array
 * @return returns maximum distance of sensor
 */
float LaserSensor::getMax(int& index)
{
	ranges = new float[541];
	robotAPI->getLaserRange(ranges);
	float temp = ranges[0];
	for (int i = 0; i < 541; i++) {
		if (temp < ranges[i]) {
			index = i;
			temp = ranges[i];
		}
	}
	return temp;
}

/*!
 * Get minimum distance information of sensor with index
 * @param index index of array
 * @return returns minimum distance of sensor
 */
float LaserSensor::getMin(int& index)
{
	ranges = new float[541];
	robotAPI->getLaserRange(ranges);
	float temp = ranges[0];
	for (int i = 0; i < 541; i++) {
		if (temp > ranges[i]) {
			index = i;
			temp = ranges[i];
		}
	}
	return temp;
}


/*!
 * Get the angle value of index
 * @param index  index of array
 * @return returns angle value
 */
float LaserSensor::getAngle(int index)
{
	return index;
}

/*!
 * Get the smallest distance on angle between start angle and end angle
 * @param startAngle firts angle value
 * @param endAngle last angle value
 * @param angle
 * @return returns the smallest distance on angle
 */
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	robotAPI->getLaserRange(ranges);
	float temp = ranges[0];
	int min_index;
	for (int i = startAngle; i < (int)endAngle; i++) {
		if (temp > ranges[i]) {
			temp = ranges[i];
			min_index = i;
		}
	}
	angle = min_index;
	return temp;

}
/*!
 * Set the API of sensor 
 * @param nAPI API
 */
void LaserSensor::setAPI(PioneerRobotAPI* nAPI)
{
	robotAPI = nAPI;
}
