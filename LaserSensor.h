/*!
 *  @file   LaserSensor.h
 *  @brief  Including Laser Sensor operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-01-20
 */
#pragma once
#include <iostream>
#include "RangeSensor.h"

class LaserSensor : public RangeSensor
{
public:
	LaserSensor() {};
	float getRange(int);
	void updateSensor(float[]);
	float getMax(int&);
	float getMin(int&);
	float getAngle(int);
	float getClosestRange(float, float, float&);
	void setAPI(PioneerRobotAPI*);
};