/*!
 *  @file   Menu.cpp
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  The class for the operations of menu system
 *
 */
#include "Menu.h"

Menu::Menu()
{
}

/*
*!
* This method printing the name of Menu and its options
*/
void Menu::print()
{
	cout << name << endl;
	for (size_t i = 0; i < options.size(); i++)
	{
		cout << (i + 1) << ". " << options.at(i) << endl;
	}
}

/*
*!
* This method adding new element to Menu's options
* @param newOpt it is a string value that holds the element that will be add to options
*/
void Menu::AddOption(string newOpt)
{
	options.push_back(newOpt);
}

/*
*!
* This method deleting the element from Menu's options
* @param index it is a integer value that hold the variable that will delete
*/
void Menu::DeleteOption(int index)
{
	options.erase(options.begin() + index);
}

/*
*!
* This method deleting the element from Menu's options
* @param index it is a integer value that hold the variable that will delete
* @return returns the value at the index
*/
string Menu::GetOption(int index)
{
	return options.at(index);
}

/*
*!
* This method gets the options of menu as a vecotor
* @return returns the options of menu
*/
vector<string> Menu::GetOptions()
{
	return options;
}

/*
*!
* This method is setting all of the options
* @param newOptions that is vector class
*/
void Menu::SetOptions(vector<string> newOptions)
{
	options = newOptions;
}