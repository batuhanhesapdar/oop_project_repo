/*!
 *  @file   Menu.h
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  The class for the operations of menu system
 *
 */
#pragma once
#include <iostream>
#include <vector>
using namespace std;

class Menu {
private:
	string name;
	vector<string> options;
public:
	Menu();
	Menu(string newname) { name = newname; };
	void print();
	void AddOption(string);
	void DeleteOption(int);
	string GetOption(int);
	vector<string> GetOptions();
	void SetOptions(vector<string>);
};