/*!
 *  @file   MenuHandler.cpp
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  The class for the operations of menu system
 *
 */

#include "MenuHandler.h"
#include <windows.h>

int MenuHandler::GetInput()
{
	int input = 0;
	cout << "Choose one : ";
	cin >> input;
	return input;
}
/*
*!
*Start the system and showing first menu that is main menu
* @param Menu this is the current state
*/
void MenuHandler::StartMenuSystem()
{
	if (controller->openAccess()) {
		status = true;
	}
	DefineOurMenus();
	MethodExecuter();
}

/*
*!
*This method define the our menus with their options
*/
void MenuHandler::DefineOurMenus() {
	mainMenu->AddOption("Connection");
	mainMenu->AddOption("Motion");
	mainMenu->AddOption("Sensor");
	mainMenu->AddOption("Quit");

	conMenu->AddOption("Connect Robot");
	conMenu->AddOption("Disconnect Robot");
	conMenu->AddOption("Quit");


	motionMenu->AddOption("Turn Left");
	motionMenu->AddOption("Turn Right");
	motionMenu->AddOption("Move Forward");
	motionMenu->AddOption("Move Backward");
	motionMenu->AddOption("Quit");

	sensorMenu->AddOption("Laser Sensor");
	sensorMenu->AddOption("Sonar Sensor");
	sensorMenu->AddOption("Quit");


	laserMenu->AddOption("Get Min Range");
	laserMenu->AddOption("Get Max Range");
	laserMenu->AddOption("Get All Ranges");
	laserMenu->AddOption("Quit");

	sonarMenu->AddOption("Get Min Range");
	sonarMenu->AddOption("Get Max Range");
	sonarMenu->AddOption("Get All Ranges");
	sonarMenu->AddOption("Quit");
}
/*
*!
*This method is decide the method that will be exucuted
* @param curMenu curMenu is a Menu class the current Menu
* @param index index is a integer value of the option that will be executed
*/
void MenuHandler::MethodExecuter()
{
	while (status) {
		mainMenu->print();
		int var = GetInput();
		switch (var) {
		case 1:
		{
			bool statusCon = true;
			while (statusCon) {
				conMenu->print();
				int varCon = GetInput();
				switch (varCon) {
				case 1:
				{
					cout << "<Connect>\n";
					cout << "Connected.\n";
					break;
				}
				case 2:
				{
					cout << "<Disconnect>\n";
					robot->disconnect();
					break;
				}
				case 3:
				{
					cout << "<Quit>\n";
					statusCon = false;
					break;
				}
				default:
				{
					cout << "Invalid Input!\n";
					break;
				}
				}
			}
			break;
		}
		case 2:
		{
			bool statusMotion = true;
			while (statusMotion) {
				motionMenu->print();
				int varMotion = GetInput();
				switch (varMotion) {
				case 1:
				{
					cout << "<Turn Left>\n";
					controller->turnLeft();
					Sleep(1000);
					controller->stopTurn();
					controller->addToPath();
					break;
				}
				case 2:
				{
					cout << "<Turn Right>\n";
					controller->turnRight();
					Sleep(1000);
					controller->stopTurn();
					controller->addToPath();
					break;
				}
				case 3: // Forward
				{
					cout << "<Move Forward>\n";
					double speedForward;
					cout << "Enter your speed: ";
					cin >> speedForward;
					cout << "Enter the time in ms: ";
					int Time;
					cin >> Time;
					controller->forward(speedForward);
					Sleep(Time);
					controller->stopMove();
					controller->addToPath();
					break;
				}
				case 4: // Backward
				{
					cout << "<Move Backward>\n";
					double speedBackward;
					cout << "Enter your speed: ";
					cin >> speedBackward;
					cout << "Enter the time in ms: ";
					int Time;
					cin >> Time;
					controller->backward(speedBackward);
					Sleep(Time);
					controller->stopMove();
					controller->addToPath();
					break;
				}
				case 5:
				{
					cout << "<Quit>\n";
					statusMotion = false;
					break;
				}
				default:
				{
					cout << "Invalid Input!\n";
					break;
				}
				}
			}
			break;
		}
		case 3:
		{
			bool statusSensor = true;
			while (statusSensor) {
				sensorMenu->print();
				int varSensor = GetInput();
				switch (varSensor) {
				case 1:
				{
					bool statusLaser = true;
					while (statusLaser) {
						laserMenu->print();
						int varLaser = GetInput();
						switch (varLaser) {
						case 1:
						{
							cout << "<Minimum Range>\n";
							int indexMinLaser;
							cout << "Min range is: " << controller->getLaser()->getMin(indexMinLaser);
							cout << " At Index: " << indexMinLaser << endl;
							break;
						}
						case 2:
						{
							cout << "<Maximum Range>\n";
							int indexMaxLaser;
							cout << "Max range is: " << controller->getLaser()->getMax(indexMaxLaser);
							cout << " At Index: " << indexMaxLaser << endl;
							break;
						}
						case 3:
						{
							cout << "<Get All Ranges>\n";
							float* rangeslaser = nullptr;
							controller->getLaser()->updateSensor(rangeslaser);
							for (int i = 0; i < 541; i++) {
								cout << controller->getLaser()->getRange(i) << endl;
							}
							break;
						}
						case 4:
						{
							cout << "<Quit>\n";
							statusSensor = false;
							statusLaser = false;
							break;
						}
						default:
						{
							cout << "Invalid Input!\n";
							break;
						}
						}
					}
					break;
				}
				case 2:
				{
					bool statusSonar = true;
					while (statusSonar) {
						sonarMenu->print();
						int varSonar = GetInput();
						switch (varSonar) {
						case 1:
						{
							cout << "<Minimum Range>\n";
							int indexMinSonar;
							cout << "Min range is: " << controller->getSonar()->getMin(indexMinSonar);
							cout << " At Index: " << indexMinSonar << endl;
							break;
						}
						case 2:
						{
							cout << "<Maximum Range>\n";
							int indexMaxSonar;
							cout << "Max range is: " << controller->getSonar()->getMax(indexMaxSonar);
							cout << " At Index: " << indexMaxSonar << endl;
							break;
						}
						case 3:
						{
							cout << "<Get All Ranges>\n";
							float* rangesSonar=nullptr;
							controller->getSonar()->updateSensor(rangesSonar);
							for (int i = 0; i < 541; i++) {
								cout << controller->getSonar()->getRange(i) << endl;
							}
							break;
						}
						case 4:
						{
							cout << "<Quit>\n";
							statusSensor = false;
							statusSonar = false;
							break;
						}
						default:
						{
							cout << "Invalid Input!\n";
							break;
						}
						}
					}
					break;
				}
				}
			}
		}
		case 4:
		{
			cout << "<Quit>\n";
			status = false;
			break;
		}
		default:
		{
			cout << "Invalid Input!\n";
			break;
		}
		}
	}
	controller->recordPathToFile();
}