/*!
 *  @file   MenuHandler.h
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  The class for the operations of menu system
 *
 */
#pragma once
#include <iostream>
#include "Menu.h"
#include "PioneerRobotInterface.h"
#include "RobotControl.h"
using namespace std;

class MenuHandler {
private:
	PioneerRobotInterface* robot = new PioneerRobotInterface;
	RobotControl* controller = new RobotControl(robot);
	Menu* mainMenu = new Menu("Main Menu");
	Menu* conMenu = new Menu("Connection Menu");
	Menu* motionMenu = new Menu("Motion Menu");
	Menu* sensorMenu = new Menu("Sensor Menu");
	Menu* laserMenu = new Menu("Laser Menu");
	Menu* sonarMenu = new Menu("Solar Menu");
	bool status = false;
public:
	int GetInput();
	void DefineOurMenus();
	void StartMenuSystem();
	void MethodExecuter();
};