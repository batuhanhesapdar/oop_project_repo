/*!
 *  @file   Node.h
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  Linked list node
 *
 */
#pragma once
#include "Pose.h"

struct Node {
	Pose pose;
	Node* next;
};