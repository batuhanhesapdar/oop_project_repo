/*!
 *  @file   Path.cpp
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  Holds the positions of the robot
 *
 */
#include "Path.h"

 /*!
   * Adds positions to path
   *
   * @param pose a position that is going to be added to path
   */
void Path::addPos(Pose pose)
{
	Node* tmp = new Node;
	tmp->pose = pose;
	tmp->next = NULL;

	if (head == NULL)
	{
		head = tmp;
		tail = tmp;
	}
	else
	{
		tail->next = tmp;
		tail = tail->next;
	}
	cout << "New Point added to path" << endl;
}

/*!
  * Prints positions in the path
  *
  */
void Path::print()
{
	cout << "The path: " << endl;
	Node* p = head;
	if (head == NULL)
	{
		cout << "NULL" << endl;
	}
	else
	{
		while (p != NULL)
		{
			cout << "X: " << p->pose.getX();
			cout << " Y: " << p->pose.getY();
			cout << " Th: " << p->pose.getTh() << endl;
			p = p->next;
		}
	}
}

/*!
  * Returns the position with the given index
  *
  * @param i index of the position
  * @return returns the related position as a reference
  */
Pose& Path::operator[](int i)
{
	int counter = 0;
	Node* p = head;
	while (p != NULL && counter != i)
	{
		p = p->next;
		counter++;
	}
	return p->pose;
}

/*!
  * Takes an index and returns the position
  *
  * @param index it is an index
  * @return returns the position with as a value
  */
Pose Path::getPos(int index)
{

	int counter = 0;
	Node* p = head;
	while (p != NULL && counter != index)
	{
		p = p->next;
		counter++;
	}
	return p->pose;
}

/*!
  * Takes an index and removes the position
  *
  * @param index it is an index
  * @return returns status of the process
  */
bool Path::removePos(int index)
{
	int counter = 0;
	Node* q = NULL;
	Node* p = head;

	while (p != NULL && counter != index) {
		q = p;
		p = p->next;
		counter++;
	}


	if (p == NULL)
	{
		cout << "The point " << index << " is not found." << endl;
		return false;
	}

	if (q == NULL) head = p->next;
	else q->next = p->next;

	delete p;
	cout << "The point " << index << " is removed from path." << endl;
	return true;
}

/*!
  * Inserts an element at a specific position to linked list
  *
  * @param index it is an index of linked list
  * @return status of the process
  */
bool Path::insertPos(int index, Pose pose)
{
	Node* prev = new Node();
	Node* curr = new Node();
	Node* newNode = new Node();
	newNode->pose = pose;

	int tempPos = 0;

	curr = head;
	if (head != NULL)
	{
		while (curr->next != NULL && tempPos != index)
		{
			prev = curr;
			curr = curr->next;
			tempPos++;
		}
		if (index == 0)
		{
			cout << "Adding at Head! " << endl;
		}
		else if (curr->next == NULL && index == tempPos + 1)
		{
			cout << "Adding at Tail! " << endl;
		}
		else if (index > tempPos + 1)
			cout << " Position is out of bounds " << endl;

		else
		{
			prev->next = newNode;
			newNode->next = curr;
			cout << "Node added at position: " << index << endl;
		}
	}
	else
	{
		head = newNode;
		newNode->next = NULL;
		cout << "Added at head as list is empty! " << endl;
	}
	return true;
}

/*!
  * Calculates length of the path
  *
  * @return returns cnt
  */
int Path::length()
{
	Node* p = head;
	int cnt = 0;

	while (p != NULL) {
		cnt++;
		p = p->next;
	}

	return cnt;
}
