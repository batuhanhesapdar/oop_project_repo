/*!
 *  @file   Path.h
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  Holds the positions of the robot
 *
 */
#pragma once
#include <iostream>
#include "Node.h"
#include "Pose.h"

using namespace std;

class Path {
private:
	Node* tail = NULL;
	Node* head = NULL;
public:
	void addPos(Pose pose);
	void print();
	Pose& operator[](int i);
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
	int length();
};