/*!
 *  @file   PioneerRobotInterface.cpp
 *  @brief 	Functions for Robot Moves and update status
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2021-01-23
 */
#include "PioneerRobotInterface.h"

/*!
 * Constructor method of RobotControl class
 *
 */
PioneerRobotInterface::PioneerRobotInterface()
{
	robotAPI->connect();
}

/*!
 * This function turning robot to left direction
 *
 */
void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	cout << "Robot is turning left..." << endl;
}

/*!
 * This function turning robot to right direction
 *
 */
void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	cout << "Robot is turning right..." << endl;
}

/*!
 * This function is moving robot to forward
 *
 * @param speed is the speed of the robot that will move
 */
void PioneerRobotInterface::forward(double speed)
{
	robotAPI->moveRobot(speed);
}

/*!
 * This function printing robot position information
 *
 */
void PioneerRobotInterface::print() const
{
	cout << "Robot Information is: " << endl;
	cout << "Robot X,Y,Th is: " << robotAPI->getX() << " " << robotAPI->getY() << " " << robotAPI->getTh() << endl;
}

/*!
 * This function is moving robot to backward
 *
 * @param speed is the speed of the robot that will move
 */
void PioneerRobotInterface::backward(double speed)
{
	robotAPI->moveRobot(-speed);
}

/*!
 * This function gets the Position of Robot
 *
 * @return returns a Pose value that is position informations of Robot
 */
Pose PioneerRobotInterface::getPose()
{
	Pose newP;
	newP.setX(robotAPI->getX());
	newP.setY(robotAPI->getY());
	newP.setTh(robotAPI->getTh());
	return newP;
}

/*!
 * This function changing robot's position
 *
 * @param pose is a Pose type value that will be became new pose of robot
 */
void PioneerRobotInterface::setPose(Pose pose)
{
	robotAPI->setPose(pose.getX(), pose.getY(), pose.getTh());
}

/*!
 * This function stops turning of robot
 *
 */
void PioneerRobotInterface::stopTurn()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	cout << "Turn Stopped." << endl;
}

/*!
 * This function stops the robot
 *
 */
void PioneerRobotInterface::stopMove()
{
	robotAPI->moveRobot(0);
	robotAPI->stopRobot();
	cout << "Robot stopped." << endl;
}

/*!
 * This function is disconecting the robot from system
 *
 */
void PioneerRobotInterface::disconnect()
{
	robotAPI->disconnect();
}

/*!
 * This function changing robot's position
 *
 * @param sensor is a RangeSensor type pointer that will be added to sensor list
 */
void PioneerRobotInterface::addSensors(RangeSensor* sensor)
{
	if (sensor == nullptr) {
		cout << "��er" << endl;
	}
	
	sensor->setAPI(robotAPI);
	sensors.push_back(sensor);
}


/*!
 * This function is updating the sensors value by calling each sensors updateSensor Methods
 *
 */
void PioneerRobotInterface::updateSensors()
{
	for (int i = 0; i < sensors.size(); i++) {
		float* temp=nullptr;
		sensors[i]->updateSensor(temp);
	}
}
