/*!
 *  @file   PioneerRobotInterface.h
 *  @brief 	Functions for Robot Moves and update status
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2021-01-23
 */
#pragma once
#include "PioneerRobotAPI.h" 
#include <iostream>
#include "RobotInterface.h"
using namespace std;

class RobotInterface;
class PioneerRobotInterface : public RobotInterface {
private:
	PioneerRobotAPI* robotAPI = new PioneerRobotAPI;
public:
	PioneerRobotInterface();
	void turnLeft();
	void turnRight();
	void forward(double);
	void print() const;
	void backward(double);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
	void disconnect();
	void addSensors(RangeSensor*);
	void updateSensors();
};
