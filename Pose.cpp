/*!
 *  @file   Pose.cpp
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  Operations on the position
 *
 */
#include "Pose.h"
#include <cmath>
#define PI 3.14159f

 /*!
   * Checks the pose equation
   *
   * @param other the position that is going to be checked
   * @return returns status of equation
   */
bool Pose::operator==(const Pose& other)
{
	return (this->x == other.x && this->y == other.y && this->th == other.th);
		
}

/*!
  * Summation of positions
  *
  * @param other the position that is going to be added
  * @return returns sum of positions
  */
Pose Pose::operator+(const Pose& other)
{
	Pose temp;
	temp.x = this->x + other.x;
	temp.y = this->y + other.y;
	temp.th = this->th + other.th;
	return temp;
}

/*!
  * Substraction of positions
  *
  * @param other the position that is going to be substracted
  * @return returns substraction of positions
  */
Pose Pose::operator-(const Pose& other)
{
	Pose temp;
	temp.x = this->x - other.x;
	temp.x = this->y - other.y;
	temp.th = this->th - other.th;
	return temp;
}

/*!
  * Summation of positions
  *
  * @param other the position that is going to be added
  * @return returns sum of positions as a reference
  */
Pose& Pose::operator+=(const Pose& other)
{
	this->x += other.x;
	this->y += other.y;
	this->th = this->th + other.th;
	return *this;
}

/*!
  * Substraction of positions
  *
  * @param other the position that is going to be substracted
  * @return returns substraction of positions as a reference
  */
Pose& Pose::operator-=(const Pose& other)
{
	this->x -= other.x;
	this->y -= other.y;
	this->th = this->th + other.th;
	return *this;
}

/*!
  * Checks the inequality
  *
  * @param other the position that is going to be checked
  * @return status of the inequality
  */
bool Pose::operator<(const Pose& other)
{
	return (sqrt(x * x + y * y) < sqrt(other.x * other.x + other.y * other.y));
}

/*!
  * Returns the position element by element
  *
  * @param x coordinate on the x-axis
  * @param y coordinate on the y-axis
  * @param th angle of the robot
  */
void Pose::getPose(float& x, float& y, float& th)
{
	x = this->x;
	y = this->y;
	th = this->th;
}

/*!
  * Assigns the position element by element
  *
  * @param x coordinate on the x-axis
  * @param y coordinate on the y-axis
  * @param th angle of the robot
  */
void Pose::setPose(float x, float y, float th)
{
	this->x = x;
	this->y = y;
	this->th = th;
}

/*!
  * Find distance between positions
  *
  * @param pos the position that is going to be compared
  * @return the distance between positions
  */
float Pose::findDistanceTo(Pose pos)
{
	float x1 = pos.x - this->x;
	float y1 = pos.y - this->y;
	return sqrt(x1 * x1 + y1 * y1);
}

/*!
  * Find angle between positions
  *
  * @param pos the position that is going to be compared
  * @return the angle between positions
  */
float Pose::findAngleTo(Pose pos)
{
	float x1 = pos.x - this->x;
	float y1 = pos.y - this->y;
	return atan2(y1, x1) * (180.f / PI);
}
