/*!
 *  @file   Pose.h
 *  @author �mer Faruk �ahin - 151220174032
 *  @date   January 2021
 *  @brief  Operations on the position
 *
 */
#pragma once
#include <iostream>
using namespace std;

class Pose {
private:
	float x=0;
	float y=0;
	float th=0;
public:
	float getX() { return x; }
	void setX(float x) { this->x = x; }
	float getY() { return y; }
	void setY(float y) { this->y = y; }
	void setTh(float th) { this->th = th; }
	float getTh() { return th; }
	bool operator==(const Pose& other);
	Pose operator+(const Pose& other);
	Pose operator-(const Pose& other);
	Pose& operator+=(const Pose& other);
	Pose& operator-=(const Pose& other);
	bool operator<(const Pose& other);
	void getPose(float& x, float& y, float& th);
	void setPose(float x, float y, float th);
	float findDistanceTo(Pose pos);
	float findAngleTo(Pose pos);
};

