/*!
 *  @file   RangeSensor.h
 *  @brief  Including Sonar Sensor operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-01-20
 */
#pragma once
#include "PioneerRobotAPI.h"
#include <iostream>


class RangeSensor
{
protected:
	float* ranges;
	PioneerRobotAPI* robotAPI;
public:
   /*!
   * A constructor for RangeSensor class
   */
	RangeSensor() {};
  /*!
   * A defualt constructor for RangeSensor class
   * @param a  pionnerRobotAPI
   * @param b  size of ranges array
   */
	RangeSensor(PioneerRobotAPI* a, int b) { robotAPI = a; ranges = new float[b]; };

	virtual float getRange(int) = 0;
	virtual void updateSensor(float[]) = 0;
	virtual float getMax(int&) = 0;
	virtual float getMin(int&) = 0;
	float& operator[](int i)
	{
		robotAPI->getSonarRange(ranges);
		if (i > ((sizeof(ranges) / sizeof(ranges[0])))) {
			return ranges[0];
		}
		return ranges[i];
	};
	virtual void setAPI(PioneerRobotAPI* current) = 0;
	virtual float getAngle(int) = 0;
	virtual float getClosestRange(float, float, float&) = 0;
};
