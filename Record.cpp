/*!
 *  @file   Record.cpp
 *  @author Berkay �nk - 152120181110
 *  @date   23th January 2021
 *  @brief  Definition of operations Record.cpp
 *
 */
#include"Record.h"

 /*!
  * Create and open file
  *
  * @return if the file is already open return false, else open file and return true
  */
bool Record::openFile()
{
	if (file.is_open()) {
		return false;
	}
	else {
		file.open(fileName, fstream::out);
		return true;
	}
}

/*!
 * Close file
 *
 * @return if the file is open close the file then return true, else return false
 */
bool Record::closeFile()
{
	if (file.is_open()) {
		file.close();
		return true;
	}
	else {
		return false;
	}
}

/*!
 * Set file name before creating file
 *
 * @param a takes name of the file then sets it to fileName
 */
void Record::setFileName(string a)
{
	fileName = a;
}

/*!
 * Reads one line from file, starts from first line
 *
 * @return if the file is open then returns to tmp else returns string()
 */
string Record::readLine()
{
	if (file.is_open()) {
		string tmp;
		ifstream command_file(this->fileName);
		getline(command_file, tmp);
		cout << "Reading Line: " << tmp << endl;
		return tmp;
	}
	else {
		return string();
	}
}

/*!
 * Writes to the one line, starts from first line
 *
 * @param b takes words or number we want to write to the file
 * @return if the file is open then returns to true else returns false
 */
bool Record::writeLine(string b)
{
	if (file.is_open()) {
		cout << "Writing Line: " << b << endl;
		file << b << endl;;
		return true;
	}
	else {
		return false;
	}

}

//Record& Record::operator<<(istream& is)
//{
	// TODO: insert return statement here
//}

//Record& Record::operator>>(istream& is)
//{
	// TODO: insert return statement here
//}
