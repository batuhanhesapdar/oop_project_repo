/*!
 *  @file   Record.h
 *  @author Berkay �nk - 152120181110
 *  @date   23th January 2021
 *  @brief  Declaration of class record
 *
 */
#pragma once
#include<string>
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<vector>
using namespace std;

class Record {
	string fileName;
	fstream file;
public:
	bool openFile();
	bool closeFile();
	void setFileName(string);
	string readLine();
	bool writeLine(string);
	Record& operator<<(istream& is);
	Record& operator>>(istream& is);
};
