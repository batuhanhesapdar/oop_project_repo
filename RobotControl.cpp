/*!
 *  @file   RobotControl.cpp
 *  @brief 	Functions for Robot Moves and Recording path
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2021-01-23
 */

#include "RobotControl.h"
#include <iostream>
using namespace std;


/*!
 * Constructor method of RobotControl class
 *
 * @param z takes a PioneerRobotInterface type pointer that will be assign to robotINT
 */
RobotControl::RobotControl(PioneerRobotInterface* z)
{
	robotINT = z;
	robotINT->addSensors(laser);
	robotINT->addSensors(sonar);
}

/*!
 * This function turning robot to left direction by checking operatorStatus and RobotInterface's turnLeft method
 *
 */
void RobotControl::turnLeft()
{
	if (operatorStatus)
	{
		robotINT->turnLeft();
	}
}

/*!
 * This function turning robot to right direction by checking operatorStatus and RobotInterface's turnRight method
 *
 */
void RobotControl::turnRight()
{
	if (operatorStatus)
	{
		robotINT->turnRight();
	}
}

/*!
 * This function is moving robot to forward by checking operatorStatus and RobotInterface's forward method
 *
 * @param speed is the speed of the robot that will move
 */
void RobotControl::forward(double speed)
{
	if (operatorStatus)
	{
		robotINT->forward(speed);
	}
}

/*!
 * This function printing robot position information by checking operatorStatus and RobotInterface's print method
 *
 */
void RobotControl::print() const
{
	if (operatorStatus)
	{
		robotINT->print();
	}
}

/*!
 * This function is moving robot to backward by checking operatorStatus and RobotInterface's backward method
 *
 * @param speed is the speed of the robot that will move
 */
void RobotControl::backward(double speed)
{
	if (operatorStatus)
	{
		robotINT->backward(speed);
	}
}

/*!
 * This function gets the Position of Robot by checking operatorStatus and RobotInterface's getPose method
 *
 * @return returns a Pose value that is position informations of Robot
 */
Pose RobotControl::getPose()
{
	if (operatorStatus)
	{
		return robotINT->getPose();
	}
	return Pose();
}

/*!
 * This function changing robot's position by checking operatorStatus and RobotInterface's setPose method
 *
 * @param pose is a Pose type value that will be became new pose of robot
 */
void RobotControl::setPose(Pose pose)
{
	if (operatorStatus)
	{
		robotINT->setPose(pose);
	}
}

/*!
 * This function stops turning of robot by checking operatorStatus and RobotInterface's stopTurn method
 *
 */
void RobotControl::stopTurn()
{
	if (operatorStatus)
	{
		robotINT->stopTurn();
	}
}

/*!
 * This function stops the robot by checking operatorStatus and RobotInterface's stopMove method
 *
 */
void RobotControl::stopMove()
{
	if (operatorStatus)
	{
		robotINT->stopMove();
	}
}

/*!
 * This function is adds a new Pose to Path 
 *
 * @return returns a bool value that holds the process status
 */
bool RobotControl::addToPath()
{
	if (operatorStatus)
	{
		path.addPos(getPose());
		return true;
	}
	return false;
}

/*!
 * This function is clearing the path by removing all poses
 *
 * @return returns a bool value that holds the process status
 */
bool RobotControl::clearPath()
{
	if (operatorStatus)
	{
		int i = 0;
		while (i != path.length())
		{
			path.removePos(i); i++;
		}
	}
	return false;
}

/*!
 * This function is recording our path information to a .txt file
 *
 * @return returns a bool value that holds the process status
 */
bool RobotControl::recordPathToFile()
{
	if (operatorStatus)
	{
		string ourPathStr = "";
		record.setFileName("PathInfo.txt");
		record.openFile();
		int i = 0;
		while (i != path.length())
		{
			ourPathStr += to_string(i) + ". node information is: \n";
			ourPathStr += "X is: " + to_string(path[i].getX()) + " ";
			ourPathStr += "Y is: " + to_string(path[i].getY()) + " ";
			ourPathStr += "Th is: " + to_string(path[i].getTh()) + " \n";
			record.writeLine(ourPathStr);
			ourPathStr = ""; i++;
		}
		record.closeFile();
	}
	return false;
}

/*!
 * This function is opens the access to use program
 *
 * @return returns a bool value that holds the process status
 */
bool RobotControl::openAccess()
{
	int num;
	bool acState;
	robotOperator.getinfo();
	num = robotOperator.encryptCode(6690);
	acState = robotOperator.checkAccessCode(num);
	robotOperator.decryptCode(num);
	robotOperator.print();

	if (acState)
	{
		operatorStatus = true;
		return true;
	}
	return false;
}

/*!
 * This function is closes the access to use program
 *
 * @param a is the access code to compare
 * @return returns a bool value that holds the process status
 */
bool RobotControl::closeAccess(int a)
{
	if (robotOperator.checkAccessCode(a))
	{
		operatorStatus = false;
		return true;
	}
	return false;
}

/*!
 * This function is gets the laser sensor
 *
 * @return returns a LaserSensor pointer of our LaserSensor
 */
LaserSensor* RobotControl::getLaser()
{
	return laser;
}

/*!
 * This function is gets the sonar sensor
 *
 * @return returns a SonarSensor pointer of our SonarSensor
 */
SonarSensor* RobotControl::getSonar()
{
	return sonar;
}

/*!
 * This function is disconecting the robot from system
 *
 */
void RobotControl::disconnect()
{
	robotINT->disconnect();
}
