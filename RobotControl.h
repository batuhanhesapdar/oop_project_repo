/*!
 *  @file   RobotControl.h
 *  @brief 	Functions for Robot Moves and Recording path
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2021-01-21
 */
#pragma once
#include "RobotInterface.h"
#include <iostream>
#include "Path.h"
#include "Record.h"
#include "LaserSensor.h"
#include "SonarSensor.h"
#include "RobotOperator.h"
#include "PioneerRobotInterface.h"

using namespace std;

class RobotControl {
	RobotInterface* robotINT;
	Path path;
	Record record;
	RobotOperator robotOperator;
	LaserSensor* laser = new LaserSensor;
	SonarSensor* sonar = new SonarSensor;
	bool operatorStatus = true;
public:
	RobotControl(PioneerRobotInterface*);
	void turnLeft();
	void turnRight();
	void forward(double);
	void print() const;
	void backward(double);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
	bool addToPath();
	bool clearPath();
	bool recordPathToFile();
	bool openAccess();
	bool closeAccess(int);
	LaserSensor* getLaser();
	SonarSensor* getSonar();
	void disconnect();
};
