/*!
 *  @file   RobotInterface.h
 *  @brief 	Abstract class for PioneerRobotInterface.h
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2021-01-23
 */
#pragma once
#include "RangeSensor.h"
#include <iostream>
#include <vector>
#include "Pose.h"

using namespace std;

class RangeSensor;

class RobotInterface {
public:
	vector<RangeSensor*> sensors;
	virtual void turnLeft() = 0;
	virtual void turnRight() = 0;
	virtual void forward(double) = 0;
	virtual void print() const = 0;
	virtual void backward(double) = 0;
	virtual Pose getPose() = 0;
	virtual void setPose(Pose) = 0;
	virtual void stopTurn() = 0;
	virtual void stopMove() = 0;
	virtual void addSensors(RangeSensor*) = 0;
	virtual void disconnect() = 0;
};
