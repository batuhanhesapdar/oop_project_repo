/*!
 *  @file   RobotOperator.cpp
 *  @author Berkay �nk - 152120181110
 *  @date   24th January 2021
 *  @brief  Definition of operations RobotOperator.cpp
 *
 */
#include"RobotOperator.h"

 /*!
  * Default constructor
  *
  */
RobotOperator::RobotOperator() : accessCode(), accessState(), accessCodeUser()
{

}

/*!
 * constructor of RobotOperator
 *
 * @param x takes our accessCode
 */
RobotOperator::RobotOperator(int x) : accessCode(x)
{
	accessCode = x;
	accessState = false;
	accessCodeUser = 15;
}

/*!
 * Gets info of the user name, surname, user's accessCode
 *
 */
void RobotOperator::getinfo()
{
	cout << "Please enter your name: ";
	cin >> name;
	cout << "Please enter your surname: ";
	cin >> surname;
	cout << "Please enter your access code: ";
	cin >> accessCodeUser;
}

/*!
 * Calls encrypt in order to encrypt code
 *
 * @param x takes our access code to encrypt
 * @return returns accessCode our integer that takes code from user
 */
int RobotOperator::encryptCode(int x)
{
	accessCode = ourEnc.encrypt(x);
	return accessCode;
}

/*!
 * Calls decrypt in order to decrypt code
 *
 * @param y takes our access code to decrypt
 * @return returns accessCode our integer that takes code
 */
int RobotOperator::decryptCode(int y)
{
	accessCode = ourEnc.decrypt(y);
	return accessCode;
}

/*!
 * Compares encrypted access code to user's access code
 *
 * @param z takes our access code to compare
 * @return returns true when user's access code same as our access code else returns false
 */
bool RobotOperator::checkAccessCode(int z)
{
	z = accessCode;
	if (z == accessCodeUser) {
		cout << "Giris Basarili!" << endl;
		accessState = true;
		return true;
	}
	else {
		cout << "Giris Basarisiz!" << endl;
		accessState = false;
		return false;
	}
}

/*!
 * Prints information about user
 *
 */
void RobotOperator::print()
{
	cout << "Name: " << name << " " << "Surname: " << surname << " " << "Authorization: " << accessState << endl;
}