/*!
 *  @file   RobotOperator.h
 *  @author Berkay �nk - 152120181110
 *  @date   24th January 2021
 *  @brief  Declaration of class RobotOperator
 *
 */
#pragma once
#include<string>
#include<iostream>
#include"Encryption.h"
using namespace std;

class RobotOperator {
	string name;
	string surname;
	unsigned int accessCodeUser;
	bool accessState;
	unsigned int accessCode;
	Encryption ourEnc;
public:
	RobotOperator(int);
	RobotOperator();
	bool checkAccessCode(int);
	void print();
	void getinfo();
	int encryptCode(int);
	int decryptCode(int);
};