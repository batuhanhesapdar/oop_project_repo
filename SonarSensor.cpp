﻿/*!
 *  @file   SonarSensor.cpp
 *  @brief  Including Sonar Sensor operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-01-20
 */
#include "SonarSensor.h"
	using namespace std;


/*!
* Get distance information of sensor with index
* @param index index of array
* @return returns distance of sensor
*/
float SonarSensor::getRange(int index)
{
	robotAPI->getSonarRange(ranges);
	return ranges[index];
}

/*!
 * Get maximum distance information of sensor with index
 * @param index index of array
 * @return returns maximum distance of sensor
 */
float SonarSensor::getMax(int& index)
{
	ranges = new float[16];
	robotAPI->getSonarRange(ranges);
	float temp = ranges[0];
	for (int i = 0; i < 16; i++) {
		if (temp < ranges[i]) {
			index = i;
			temp = ranges[i];
		}
	}
	return temp;

}

/*!
 * Get minimum distance information of sensor with index
 * @param index index of array
 * @return returns minimum distance of sensor
 */
float SonarSensor::getMin(int& index)
{
	ranges = new float[16];
	robotAPI->getSonarRange(ranges);
	float temp = ranges[0];
	for (int i = 0; i < 16; i++) {
		if (temp > ranges[i]) {
			index = i;
			temp = ranges[i];
		}
	}
	return temp;
}

/*!
 * Uploads the robot's current sensor distance values to the ranges array.
 * @param hold[] the array of current sensor distance
 *
 */
void SonarSensor::updateSensor(float hold[])
{
	ranges = new float[16];
	hold = new float[16];
	robotAPI->getSonarRange(ranges);
	for (int i = 0; i < 16; i++) {
		hold[i] = ranges[i];
	}

}


/*!
 * Get the angle value of index
 * @param index  index of array
 * @return returns angle value
 */
float SonarSensor::getAngle(int index)
{
	if (index == 0 || index == 15) {

		return 90;

	}

	else if (index == 7 || index == 8)
	{
		return -90;
	}

	else if ((index < 7 && index > 0))
	{

		return 50 - 20 * ((index % 8) - 1);

	}

	else if (index < 15 && index > 8)
	{
		return 50 - 20 * (abs((index % 8) - 7) - 1);

	}
	else {
		return 0;
	}
}

/*!
 * Get the smallest distance on angle between start angle and end angle
 * @param startAngle firts angle value
 * @param endAngle last angle value
 * @param angle
 * @return returns the smallest distance on angle
 */
float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	robotAPI->getLaserRange(ranges);
	float temp = ranges[0];
	int min_index;
	for (int i = startAngle; i < (int)endAngle; i++) {
		if (temp > ranges[i]) {
			temp = ranges[i];
			min_index = i;
		}
	}
	angle = min_index;
	return temp;

}
/*!
 * Set the API of sensor 
 * @param kAPI API
 */
void SonarSensor::setAPI(PioneerRobotAPI* kAPI)
{
	robotAPI = kAPI;
}