/*!
 *  @file   SonarSensor.h
 *  @brief  Including Sonar Sensor operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-01-20
 */
#pragma once
#include "RangeSensor.h"
#include <iostream>
#include <cmath>
class SonarSensor : public RangeSensor
{
public:
	SonarSensor() {};
	float getRange(int);
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float[]);
	float getAngle(int);
	float getClosestRange(float, float, float&);
	void setAPI(PioneerRobotAPI*);
};